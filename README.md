# C Game
Just learning C by making a game engine.

# Build
In Unix run build.sh
I'm not sure how Windows works.

# Assets
1. Textures:
	* Currently uses raw data types with the width stored in the file name.
	* The height of the image is the length of the file in bytes divided by 3, divided by the width.
2. Sounds:
	* Using [this](https://github.com/HumanGamer/libsfxplus) audio lib.
