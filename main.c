#include <GL/glut.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>
#include "types.h"
#include <sfxplus/sfxplus.h>

const int windowWidth = 400;
const int windowHeight = 240;
const double tps = 20.0;
const int delay = (1.0 / tps) * 1000.0;
long lastLen;

long concatenate(long x, long y)
{
    long pow = 10;
    while (y >= pow)
        pow *= 10;
    return x * pow + y;
}

long getTime()
{
    struct timeval time;
    gettimeofday(&time, NULL);
    return concatenate(time.tv_sec, time.tv_usec);
}

char *readFileBytes(const char *name)
{
    FILE *fl = fopen(name, "r");
    fseek(fl, 0, SEEK_END);
    lastLen = ftell(fl);
    char *ret = malloc(lastLen);
    fseek(fl, 0, SEEK_SET);
    fread(ret, 1, lastLen, fl);
    fclose(fl);
    return ret;
}

Texture loadTexture(const char *file)
{
    bool doit = 0;
    bool found = 0;

    int length = 0;

    for (int i = 0; i < strlen(file); i++)
    {
        if ((&file[i])[0] == '.')
        {
            doit = !doit;
            if (length == 0)
            {
                length = strlen(&file[i]) - 6;
            }
        }
    }

    char num[length];
    int index = 0;

    for (int i = 0; i < strlen(file); i++)
    {
        if ((&file[i])[0] == '.')
        {
            doit = !doit;
            found = 1;
        }

        if (!found && doit)
        {
            num[index] = (&file[i])[0];
            index++;
        }

        found = 0;
    }

    // Must be called before lastLen is used because readFileBytes sets lastLen to the length of the bytes.
    const char *fileBytes = readFileBytes(file);

    const GLsizei w = (int)strtol(num, (char **)NULL, 10);
    const GLsizei h = (lastLen / 3) / w;

    Texture textureID = 0;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, fileBytes);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    return textureID;
}

void drawTex(Texture tex, int i, int j, int width, int height)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-2, 2, 2, -2, 2, -2);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, tex);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0);

    float wFraction = 4.0f / (float)windowWidth;
    float hFraction = 4.0f / (float)windowHeight;

    float xfact = wFraction * i;
    float yfact = hFraction * j;
    float wfact = wFraction * width;
    float hfact = hFraction * height;

    float left   = xfact - 2;
    float right  = xfact + wfact - 2;
    float top    = yfact - 2;
    float bottom = yfact + hfact - 2;

    // Top left
    glVertex2f(left, top);
    glTexCoord2f(1, 0);

    // Top right
    glVertex2f(right, top);
    glTexCoord2f(1, 1);

    // Bottom right
    glVertex2f(right, bottom);
    glTexCoord2f(0, 1);

    // Bottom left
    glVertex2f(left, bottom);
    glEnd();
}

Texture texture = 0;
void renderLoop()
{

    glClearColor(0.0f, 0.3f, 0.2f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    drawTex(texture, 200, 100, 107, 107);

    glutSwapBuffers();
}

void gameLoop()
{

    // Run loop again...
    glutTimerFunc(delay, gameLoop, 0);
}

/* Main function: GLUT runs as a console application starting at main()  */
int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB); //GLUT_RGBA
    glutInitWindowSize(windowWidth, windowHeight);
    //glutInitWindowPosition(50, 50);
    glutCreateWindow("Test Game");
    texture = loadTexture("sleep.107.data");
    //glutSetWindowTitle("Something Else");
    glutTimerFunc(0, gameLoop, 0);
    glutDisplayFunc(renderLoop);
    glutMainLoop();

    return 0;
}